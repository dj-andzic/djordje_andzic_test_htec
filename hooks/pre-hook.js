const jsonConcat = require('json-concat');

const localizationSourceFilesENGB = [
  './i18n/translations.en-gb.json'
];

const localizationSourceFilesENUS = [
  './i18n/translations.en-us.json'
];

function mergeAndSaveJsonFiles(src, dest) {
  jsonConcat({ src, dest },
    function (res) {
      console.log('Localization files successfully merged!');
    }
  );
}

// Merge all localization files into one
mergeAndSaveJsonFiles(localizationSourceFilesENGB, "./i18n/en-gb.json");
mergeAndSaveJsonFiles(localizationSourceFilesENUS, "./i18n/en-us.json");
