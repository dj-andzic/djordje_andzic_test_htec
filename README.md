# DjordjeAndzicTestHtec

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

## Prerequisites
 - [Node.js](https://nodejs.org/en/download/) version 12.8.0 (should be fine from 10.13 and later).
 - Angular CLI  - `npm install -g @angular/cli`

## Development server

Run `npm start` (not `ng serve`) for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
