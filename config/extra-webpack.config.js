const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  plugins: [
    new CopyWebpackPlugin(
      [
        {
          from: '../config/app-config.json',
          to: 'config'
        },
        {
          from: '../../i18n',
          to: 'config/i18n'
        },
        {
          from: './src/favicon.ico'
        }
      ]
    )
  ]
};
