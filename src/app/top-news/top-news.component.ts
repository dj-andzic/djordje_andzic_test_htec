import {Component} from '@angular/core';

import {takeUntil} from "rxjs/operators";

import {SharedStoreSandbox} from "../shared/store/shared-store.sandbox";
import {ConfigService} from "../app-config.service";
import {BaseNewsComponent} from "../shared/components/extensibles/base-news.component";

@Component({
  selector: 'app-top-news',
  templateUrl: './top-news.component.html',
  styleUrls: ['./top-news.component.scss']
})
export class TopNewsComponent extends BaseNewsComponent {
  public country: any;

  constructor(
    public sharedStoreSandbox: SharedStoreSandbox,
    private configService: ConfigService
  ) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    this.sharedStoreSandbox.$news
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(news => {
        this.news = news;
      });

    this.sharedStoreSandbox.$selectedLanguage
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(countryCode => {
        this.sharedStoreSandbox.getNews();
        const languages = this.configService.get('localization').languages;

        for (let i = 0; i < languages.length; i++) {
          if (countryCode === languages[i].code) {
            this.country = { country: languages[i].country };
            break;
          }
        }
      });
  }
}
