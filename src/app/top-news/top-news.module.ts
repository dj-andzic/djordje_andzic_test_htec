import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopNewsComponent } from './top-news.component';
import {TopNewsRoutingModule} from "./top-news-routing.module";
import {SharedComponentsModule} from "../shared/components/shared-components.module";
import {TranslateModule} from "@ngx-translate/core";



@NgModule({
  declarations: [TopNewsComponent],
  imports: [
    CommonModule,
    TopNewsRoutingModule,
    TranslateModule,
    SharedComponentsModule
  ]
})
export class TopNewsModule { }
