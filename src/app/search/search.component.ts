import {
  AfterViewInit,
  Component,
  ElementRef,
  ViewChild
} from '@angular/core';
import {fromEvent} from "rxjs";
import {debounceTime, distinctUntilChanged, map, takeUntil, tap} from "rxjs/operators";
import {TranslateService} from "@ngx-translate/core";
import {SharedStoreSandbox} from "../shared/store/shared-store.sandbox";
import {ConfigService} from "../app-config.service";
import {BaseNewsComponent} from "../shared/components/extensibles/base-news.component";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent extends BaseNewsComponent implements AfterViewInit {
  @ViewChild('searchInput') searchInput: ElementRef;
  public country: any;
  public searchInputPlaceholder: '';
  private searchTerm = '';

  constructor(
    public sharedStoreSandbox: SharedStoreSandbox,
    private configService: ConfigService,
    private translate: TranslateService
  ) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    this.sharedStoreSandbox.clearNews();
    this.searchInputPlaceholder = this.translate.instant('SEARCH_TERM');

    this.sharedStoreSandbox.$news
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(news => {
        this.news = news;
      });

    this.sharedStoreSandbox.$selectedLanguage
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(countryCode => {
        const languages = this.configService.get('localization').languages;

        for (let i = 0; i < languages.length; i++) {
          if (countryCode === languages[i].code) {
            this.country = { country: languages[i].country };
            break;
          }
        }

        const trimmedSearchTerm = this.trimWhiteSpace(this.searchTerm);

        if (this.hasMoreThenTwoChars(trimmedSearchTerm)) {
          this.sharedStoreSandbox.getNews({ q: trimmedSearchTerm });
        }
      });
  }

  ngAfterViewInit(): void {
    const userInput = fromEvent(this.searchInput.nativeElement, 'input')
      .pipe(
        map((e: KeyboardEvent) => {
          const text = (e.target as HTMLInputElement).value;
          return this.trimWhiteSpace(text);
        }),
        tap(text => {
          this.searchTerm = text;
        }),
        debounceTime(500),
        distinctUntilChanged()
      );

    userInput
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe((searchTerm: string) => {
        if (this.hasMoreThenTwoChars(searchTerm)) {
          this.sharedStoreSandbox.getNews({ q: searchTerm });
        }
      });
  }

  private trimWhiteSpace(text: string): string {
    return text.trim().replace(/\s{2,}/g, ' ');
  }

  private hasMoreThenTwoChars(text: string): boolean {
    if (text.length > 2) {
      return true;
    } else {
      this.sharedStoreSandbox.clearNews();
      return false;
    }
  }


  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
    this.sharedStoreSandbox.clearNews();
  }
}
