import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import {TranslateModule} from "@ngx-translate/core";
import {SharedComponentsModule} from "../shared/components/shared-components.module";
import {AngularMaterialComponentsModule} from "../shared/components/angular-material-components/angular-material-components.module";


@NgModule({
  declarations: [SearchComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    TranslateModule,
    SharedComponentsModule,
    AngularMaterialComponentsModule
  ]
})
export class SearchModule { }
