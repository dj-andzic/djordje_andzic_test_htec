import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {
  Observable,
  Subject,
  throwError
} from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public configLoadedSubject = new Subject<any>();
  private config: any;

  constructor(private http: HttpClient) { }

  public load() {
    const { protocol, hostname, port } = window.location;
    const requestHost = `${protocol}//${hostname}:${port}`;

    return new Promise((resolve, reject) => {
      this.http.get(`${requestHost}/config/app-config.json`)
        .pipe(
          catchError((error: any) => {
            return throwError(error.json().error || 'Server error');
          }))
        .subscribe((data) => {
          this.config = data;
          this.configLoadedSubject.next(this.config);
          resolve(true);
        });
    });
  }

  public getConfig(): Observable<any> {
    return this.configLoadedSubject.asObservable();
  }

  /**
   * Returns configuration value based on given key
   *
   * @param key
   */
  public get(key: any) {
    return this.config[key];
  }
}
