import { Injectable }        from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable }        from 'rxjs';
import { TranslateService }  from '@ngx-translate/core';

import { LoggerService }     from '../core';

@Injectable({
  providedIn: 'root'
})
export class ErrorResponseHandler {
  constructor(
    private translate:    TranslateService,
    private logger:       LoggerService
  ) {}

  /**
   * Method used to determine which specific error handle should be triggered
   *
   * @param error        - error object sent from the backend
   * @param errorHandler - custom error handling function to be used instead of the default one
   */
  public handleError(error: HttpErrorResponse, errorHandler?: Function): Observable<any> {
    const errorData = {
      errorResponse: error,
      errorHandler
    };

    switch (errorData.errorResponse.status) {
      case 400:
        return this.handleBadRequest(errorData);
        break;

      case 401:
        return this.handleUnauthorized(errorData);
        break;

      case 403:
        return this.handleForbidden(errorData);
        break;

      case 404:
        return this.handleNotFound(errorData);
        break;

      case 500:
        return this.handleServerError(errorData);
        break;

      default:
        break;
    }
  }

  /**
   * Shows notification errors when server response status is 400
   *
   * @param errorData
   */
  private handleBadRequest(errorData: any): any {
    this.logger.error('Bad request');

    if (errorData.errorHandler) {
      return errorData.errorHandler();
    } else {
      return null;
    }
  }

  /**
   * Shows notification errors when server response status is 401
   *
   * @param errorData
   */
  private handleUnauthorized(errorData: any): any {
    this.logger.error('Unauthorized request');

    if (errorData.errorHandler) {
      return errorData.errorHandler();
    } else {
      return null;
    }
  }

  /**
   * Shows notification errors when server response status is 403
   *
   * @param errorData
   */
  private handleForbidden(errorData: any): any {
    this.logger.error('Forbidden request');

    if (errorData.errorHandler) {
      return errorData.errorHandler();
    } else {
      return null;
    }
  }

  /**
   * Shows notification errors when server response status is 404
   *
   * @param errorData
   */
  private handleNotFound(errorData: any): any {
    this.logger.error('Requested data not found');

    if (errorData.errorHandler) {
      return errorData.errorHandler();
    } else {
      return null;
    }
  }

  /**
   * Shows notification errors when server response status is 500
   *
   * @param errorData
   */
  private handleServerError(errorData: any): any {
    this.logger.error('Server error');

    if (errorData.errorHandler) {
      return errorData.errorHandler();
    } else {
      return null;
    }
  }
}
