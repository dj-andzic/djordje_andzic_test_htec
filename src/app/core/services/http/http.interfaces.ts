/**
 * Taken from the HttpClient declaration file
 */
import {IArticle} from "../../../shared/interfaces";

export type RequestOptions = any | null;

export type RequestBody = any | null;

export interface INewsApiResponse {
  status: number;
  totalResults: number;
  articles: IArticle[];
}

export interface ErrorResponse {
  status: string;
  code: string;
  message: string;
}
