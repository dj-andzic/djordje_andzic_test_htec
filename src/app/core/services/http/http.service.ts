import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import {
  RequestOptions,
  RequestBody
}                     from './http.interfaces';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  public constructor(
    protected http: HttpClient
  ) {}

  public get(url: string, options?: RequestOptions): Observable<any> {
    return this.http.get(url, options);
  }

  public post(url: string, body: RequestBody, options: RequestOptions): Observable<any> {
    return this.http.post(url, body, options);
  }

  public put(url: string, body: RequestBody, options: RequestOptions): Observable<any> {
    return this.http.put(url, body, options);
  }

  public delete(url: string, options: RequestOptions): Observable<any> {
    return this.http.delete(url, options);
  }

  public patch(url: string, body: RequestBody, options: RequestOptions): Observable<any> {
    return this.http.patch(url, body, options);
  }

  public head(url: string, options: RequestOptions): Observable<any> {
    return this.http.head(url, options);
  }

  public options(url: string, options: RequestOptions): Observable<any> {
    return this.http.options(url, options);
  }
}
