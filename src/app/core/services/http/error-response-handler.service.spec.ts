import { TestBed } from '@angular/core/testing';

import { ErrorResponseHandler } from './error-response-handler.service';

describe('ErrorResponseHandler', () => {
  let service: ErrorResponseHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorResponseHandler);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
