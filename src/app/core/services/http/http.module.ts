import { CommonModule }         from '@angular/common';
import {
  NgModule,
  ModuleWithProviders
}                               from '@angular/core';
import {HTTP_INTERCEPTORS} from "@angular/common/http";

import { HttpService }          from './http.service';
import { ErrorResponseHandler } from './error-response-handler.service';
import {
  AuthorizationInterceptor,
  BaseUrlInterceptor,
  LoggerInterceptor
} from "../../interceptors";

@NgModule({
  imports: [CommonModule]
})
export class HttpServiceModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: HttpServiceModule,

      providers: [
        HttpService,
        ErrorResponseHandler,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: BaseUrlInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthorizationInterceptor,
          multi: true
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoggerInterceptor,
          multi: true
        }
      ]
    };
  }
}
