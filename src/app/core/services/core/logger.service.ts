import { Injectable } from '@angular/core';
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  public _isProduction: boolean = environment.production;

  constructor() {}

  public get isProduction(): boolean {
    return this._isProduction;
  }

  public set isProduction(value: boolean) {
    this._isProduction = value;
  }

  public log(...args): void {
    this.writeLog('color:blue;', '[LOG]', ...args);
  }

  public warn(...args): void {
    this.writeLog('color:orange;font-weight:bold', '[WARNING]', ...args);
  }

  public error(...args): void {
    this.writeLog('color:red;font-weight:bold', '[ERROR]', ...args);
  }

  public debug(...args): void {
    this.writeLog('color:#7100a1;font-weight:bold', '[DEBUG]', ...args);
  }

  public routeLog(...args): void {
    this.writeLog('color:#0089A2;', '[ROUTE]', ...args);
  }

  public requestLog(...args): void {
    this.writeLog('color:green;', '[REQUEST]', ...args);
  }

  private writeLog(style: string, type, ...args) {
    if (!this.isProduction) {
      args.unshift(style);
      args.unshift(`%c→ DJ ${type} - %s`);
      console.log(...args);
    }
  }
}
