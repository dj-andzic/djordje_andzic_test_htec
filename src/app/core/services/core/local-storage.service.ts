import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  // prefix for keys in local storage
  private keyPrefix;
  // linker between prefix and provided key, eg: gm.myNewKey
  private keyLink;

  constructor() {
    this.keyPrefix = 'dj';
    this.keyLink = '.';
  }

  /**
   * Set value to local storage
   * @param {string} key
   * @param {object} value
   */
  public set(key: string, value: any): void {
    localStorage.setItem(this.prefixKey(key), JSON.stringify(value));
  }

  /**
   * Get value from local storage
   * @param {string} key
   * @returns {Object|Array|string|number|*}
   */
  public get(key: string): any {
    const value = localStorage.getItem(this.prefixKey(key));

    if (value) {
      return JSON.parse(value);
    }
    return undefined;
  }

  /**
   * Remove value from local storage
   * @param {string} key
   */
  public remove(key: string): void {
    localStorage.removeItem(this.prefixKey(key));
  }

  /**
   * Prefix the given key with keyPrefix string + keyLink symbol
   * @param {string} key
   * @returns {string}
   */
  private prefixKey(key: string): string {
    const prefixAndLink = this.keyPrefix + this.keyLink;

    if (key.indexOf(prefixAndLink) === 0) {
      return key;
    }
    return prefixAndLink + key;
  }
}
