import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';

import { LoggerService }       from './logger.service';
import { LocalStorageService } from "./local-storage.service";

const PROVIDERS = [
  LoggerService,
  LocalStorageService
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: PROVIDERS
})
export class CoreModule { }
