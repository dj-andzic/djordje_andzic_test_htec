import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {UtilService} from "../../shared/services/util.service";

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {

  constructor(private utilService: UtilService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // For fetching config and translation files
    const jsonRegExp = /(.json)/i;

    if (!jsonRegExp.test(request.url)) {
      return this.createUrl('baseApiUrl', request.url, request, next);
    }

    return next.handle(request);
  }

  private createUrl(
    urlType: string,
    endpoint: string,
    request: HttpRequest<any>,
    next: HttpHandler
  ) {
    const baseUrl = this.utilService[urlType];
    const requestUrl = `${baseUrl}${endpoint}`;
    const fullRequestUrl = this.utilService['countryApiUrl'](requestUrl);

    const apiRequest = request.clone({ url: fullRequestUrl });
    return next.handle(apiRequest);
  }
}
