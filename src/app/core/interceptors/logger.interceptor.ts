import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {LoggerService} from "../services/core";

@Injectable()
export class LoggerInterceptor implements HttpInterceptor {

  constructor(private loggerService: LoggerService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loggerService.requestLog(`${request.method}: ${request.urlWithParams}`);
    return next.handle(request);
  }
}
