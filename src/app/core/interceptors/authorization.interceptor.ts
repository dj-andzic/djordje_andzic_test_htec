import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {ConfigService} from "../../app-config.service";

@Injectable()
export class AuthorizationInterceptor implements HttpInterceptor {

  constructor(private configService: ConfigService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const jsonRegExp = /(.json)/i;

    if (!jsonRegExp.test(request.url)) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.configService.get('server').apiKey}`
        }
      });
    }

    return next.handle(request);
  }
}
