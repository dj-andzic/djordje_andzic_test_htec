import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TranslateModule } from "@ngx-translate/core";

import { HeaderComponent } from './header/header.component';
import { AngularMaterialComponentsModule } from "./angular-material-components/angular-material-components.module";
import { NavigationMenuComponent } from './header/navigation-menu/navigation-menu.component';
import { LanguageMenuComponent } from './header/language-menu/language-menu.component';
import {SharedStoreSandbox} from "../store/shared-store.sandbox";
import { NewsGridComponent } from './news-grid/news-grid.component';
import { NewsCardComponent } from './news-card/news-card.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import { NotFoundComponent } from './not-found/not-found.component';
import { ArticleLayoutComponent } from './article-layout/article-layout.component';
import { SidenavComponent } from './sidenav/sidenav.component';

@NgModule({
  declarations: [
    HeaderComponent,
    NavigationMenuComponent,
    LanguageMenuComponent,
    NewsGridComponent,
    NewsCardComponent,
    NotFoundComponent,
    ArticleLayoutComponent,
    SidenavComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialComponentsModule,
    TranslateModule,
    RouterModule,
    FlexLayoutModule
  ],
  exports: [
    HeaderComponent,
    NewsGridComponent,
    NewsCardComponent,
    AngularMaterialComponentsModule,
    ArticleLayoutComponent,
    SidenavComponent
  ],
  providers: [SharedStoreSandbox]
})
export class SharedComponentsModule { }
