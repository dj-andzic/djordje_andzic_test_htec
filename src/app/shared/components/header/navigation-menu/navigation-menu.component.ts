import { Component, OnInit } from '@angular/core';
import {SharedStoreSandbox} from "../../../store/shared-store.sandbox";

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {

  constructor(private sharedStoreSandbox: SharedStoreSandbox) { }

  ngOnInit(): void {
  }

  public toggleSideBar(): void {
    this.sharedStoreSandbox.toggleSideBar();
  }
}
