import {Component} from '@angular/core';

import {Store} from "@ngrx/store";
import {takeUntil} from "rxjs/operators";

import {GeneralState} from "../../../store/reducers";
import {ConfigService} from "../../../../app-config.service";
import {ILanguages} from "../../../interfaces";
import {SharedStoreSandbox} from "../../../store/shared-store.sandbox";
import {LocalStorageService} from "../../../../core/services/core";
import * as fromConstants from "../../../constants";
import {BaseComponent} from "../../extensibles/base.component";

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.scss']
})
export class LanguageMenuComponent extends BaseComponent {
  public languages: ILanguages[] = [];
  public selectedLanguage: string;
  public currentUrl: string;

  constructor(
    private store: Store<GeneralState>,
    private configService: ConfigService,
    public sharedStoreSandbox: SharedStoreSandbox,
    private localStorageService: LocalStorageService
  ) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    this.languages = this.configService.get('localization').languages;

    this.sharedStoreSandbox.$selectedLanguage
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(selectedLanguage => {
        this.selectedLanguage = selectedLanguage;
      });

    this.sharedStoreSandbox.$currentUrl
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(currentUrl => {
        this.currentUrl = currentUrl;
      });
  }

  public setLanguage($event, lang): void {
    this.localStorageService.set(fromConstants.LC_KEY_DEFAULT_APP_LANG, lang.code);
    this.sharedStoreSandbox.setLanguage(lang.code);
  }

  public isLanguageSelectionDisabled(language): boolean {
    return this.selectedLanguage === language.code || (this.currentUrl && this.currentUrl.includes('/article'));
  }
}
