import {Subject} from "rxjs";
import {OnDestroy, OnInit} from "@angular/core";
import {SharedStoreSandbox} from "../../store/shared-store.sandbox";

export class BaseComponent implements OnInit, OnDestroy {
  public ngUnsubscribe$: Subject<void> = new Subject<void>();

  constructor(public sharedStoreSandbox: SharedStoreSandbox) {}

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
    this.sharedStoreSandbox.clearNews();
  }
}
