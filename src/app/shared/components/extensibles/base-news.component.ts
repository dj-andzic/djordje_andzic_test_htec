import {BaseComponent} from "./base.component";
import {IArticle} from "../../interfaces";

export class BaseNewsComponent extends BaseComponent{
  public news: IArticle[] = [];
}
