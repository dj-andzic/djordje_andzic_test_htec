import {Component, Input, OnInit} from '@angular/core';
import {IArticle} from "../../interfaces";
import {SharedStoreSandbox} from "../../store/shared-store.sandbox";

@Component({
  selector: 'app-article-layout',
  templateUrl: './article-layout.component.html',
  styleUrls: ['./article-layout.component.scss']
})
export class ArticleLayoutComponent implements OnInit {
  @Input() article: IArticle;
  constructor(private sharedStoreSandbox: SharedStoreSandbox) { }

  ngOnInit(): void {
  }

  public goBack(): void {
    this.sharedStoreSandbox.goBack();
  }
}
