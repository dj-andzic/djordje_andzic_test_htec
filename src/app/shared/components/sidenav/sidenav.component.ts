import { Component, OnInit } from '@angular/core';
import {SharedStoreSandbox} from "../../store/shared-store.sandbox";

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  constructor(private sharedStoreSandbox: SharedStoreSandbox) { }

  ngOnInit(): void {
  }

  public toggleSideBar(): void {
    this.sharedStoreSandbox.toggleSideBar();
  }
}
