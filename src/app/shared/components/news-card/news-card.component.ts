import {Component, Input, OnInit} from '@angular/core';
import {IArticle} from "../../interfaces";

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {
  @Input() article: IArticle;

  constructor() { }

  ngOnInit(): void {
  }

}
