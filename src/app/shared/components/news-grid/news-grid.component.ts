import {Component, Input, OnChanges} from '@angular/core';

import {IArticle, ICategorizedArticles} from "../../interfaces";
import * as fromConstants from '../../constants';
import {BaseComponent} from "../extensibles/base.component";
import {SharedStoreSandbox} from "../../store/shared-store.sandbox";

@Component({
  selector: 'app-news-grid',
  templateUrl: './news-grid.component.html',
  styleUrls: ['./news-grid.component.scss']
})
export class NewsGridComponent extends BaseComponent implements OnChanges {
  @Input() news: IArticle[];
  @Input() sortByCategory: boolean;
  public filteredNews: ICategorizedArticles[] = [];

  constructor(public sharedStoreSandbox: SharedStoreSandbox) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
  }

  ngOnChanges(c): void {
    if (this.sortByCategory) {
      this.filteredNews.length = 0;

      fromConstants.CATEGORIES.forEach(category => {
        this.filteredNews.push({
          category,
          articles: []
        });
      });

      this.filteredNews = this.filteredNews.map(filteredArticle => {
        return {
          category: filteredArticle.category,
          articles: this.news.filter(article => article.category === filteredArticle.category)
        }
      });
    }
  }
}
