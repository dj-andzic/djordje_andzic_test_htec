export interface ILanguages {
  name: string;
  shortName: string;
  code: string;
  culture: string;
}

export interface IArticleSource {
  id: string;
  name: string;
}

export interface IArticle {
  source: IArticleSource;
  author: string;
  title: string;
  description: string;
  url: string;
  urlToImage: string;
  publishedAt: string;
  content: string;
  articleLink?: string;
  category?: string;
}

export interface ICategorizedArticles {
  category: string;
  articles: IArticle[];
}

export interface IGetNewsQueryParams {
  category?: string;
  q?: string;
  pageSize?: number;
}
