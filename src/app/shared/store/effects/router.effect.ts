import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';

import * as fromRouterActions from '../actions/router.action';

@Injectable()
export class RouterEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {}

  /**
   * Navigate effect
   */
  // @ts-ignore
  @Effect({ dispatch: false })
  private navigate$ = this.actions$.pipe(
    ofType(fromRouterActions.GO),
    map((action: fromRouterActions.GoAction) => action.payload),
    tap(({path, query: queryParams, extras}) => {
      this.router.navigate(path, {queryParams, ...extras});
    })
  );

  /**
   * Navigate Back effect
   */
  // @ts-ignore
  @Effect({ dispatch: false })
  private navigateBack$ = this.actions$.pipe(
    ofType(fromRouterActions.BACK),
    tap(() => this.location.back())
  );

  /**
   * Navigate Forward effect
   */
  // @ts-ignore
  @Effect({ dispatch: false })
  private navigateForward$ = this.actions$.pipe(
    ofType(fromRouterActions.FORWARD),
    tap(() => this.location.forward())
  );
}
