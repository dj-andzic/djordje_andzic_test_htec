import { Injectable } from '@angular/core';

import {
  Effect,
  Actions,
  ofType
} from '@ngrx/effects';
import {
  Action,
  Store
} from '@ngrx/store';
import {
  Observable,
  of
} from 'rxjs';
import {
  catchError,
  map,
  switchMap
} from 'rxjs/operators';

import * as fromNewsActions from '../actions/news.action';
import * as fromNewsReducer  from '../reducers/news.reducer';
import { NewsService } from "../../services/news.service";
import { ErrorResponseHandler } from "../../../core/services/http";

@Injectable()
export class NewsEffects {

  constructor(
    private actions$: Actions,
    private store: Store<fromNewsReducer.NewsState>,
    private newsService: NewsService,
    private errorResponseHandler: ErrorResponseHandler
  ) {}
  // @ts-ignore
  @Effect()
  private getNews$: Observable<Action> = this.actions$.pipe(
    ofType(fromNewsActions.NewsActionTypes.GET_NEWS),
    map((action: fromNewsActions.GetNewsAction) => action.payload),
    switchMap(state => {
      return this.newsService.getNews(state)
        .pipe(
          map(response => {
            const category = state && state.category ? state.category : '';
            return new fromNewsActions.GetNewsSuccessAction({ articles: response, category })
          }),
          catchError(error => of(new fromNewsActions.GetNewsFailAction(error)))
        );
    })
  );
  // @ts-ignore
  @Effect({ dispatch: false })
  private getNewsFail$: any = this.actions$.pipe(
    ofType(fromNewsActions.NewsActionTypes.GET_NEWS_FAIL),
    map((action: fromNewsActions.GetNewsFailAction) => {
      return this.errorResponseHandler.handleError(action.payload);
    })
  );

}
