import { NewsEffects } from "./news.effect";
import { RouterEffects } from "./router.effect";

export const effects: any[] = [
  NewsEffects,
  RouterEffects
];

export * from './news.effect';
export * from './router.effect';
