import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';

import { RouterStateUrl } from '../reducers/router.reducer';

export const getRouterState = createFeatureSelector<RouterReducerState<RouterStateUrl>>
('router');

export const getUrl = createSelector(
  getRouterState,
  (router: RouterReducerState<RouterStateUrl>): string => {
    return router && router.state && router.state.url;
  }
);

export const getUrlParams = createSelector(
  getRouterState,
  (router: RouterReducerState<RouterStateUrl>): object => {
    return router && router.state && router.state.params;
  }
);

export const getQueryParams = createSelector(
  getRouterState,
  (router: RouterReducerState<RouterStateUrl>): object => {
    return router && router.state && router.state.queryParams;
  }
);
