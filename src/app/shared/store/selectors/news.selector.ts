import {
  createFeatureSelector,
  createSelector
} from '@ngrx/store';

import * as fromNews from '../reducers/news.reducer';
import {
  GeneralState
} from '../reducers';

export const getNewsState = createFeatureSelector<GeneralState, fromNews.NewsState>(
  'news'
);

export const getArticles = createSelector(
  getNewsState,
  (state: fromNews.NewsState) => state.articles
);

export const newsLoading = createSelector(
  getNewsState,
  (state: fromNews.NewsState) => state.newsLoading
);

export const newsLoaded = createSelector(
  getNewsState,
  (state: fromNews.NewsState) => state.newsLoaded
);
