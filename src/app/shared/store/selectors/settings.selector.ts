import {
  createFeatureSelector,
  createSelector
} from '@ngrx/store';

import * as fromSettings from '../reducers/settings.reducer';
import {
  GeneralState
} from '../reducers';

export const getSettingsState = createFeatureSelector<GeneralState, fromSettings.SettingsState>(
  'settings'
);

export const getSelectedLanguage = createSelector(
  getSettingsState,
  (state: fromSettings.SettingsState) => state.selectedLanguage
);

export const getSideBarOpened = createSelector(
  getSettingsState,
  (state: fromSettings.SettingsState) => state.sideBarOpened
);
