import { Action } from '@ngrx/store';

import { UtilService } from "../../services/util.service";

export const SettingsActionTypes = {
  SET_LANGUAGE:   UtilService.checkActionType('[Settings] Set Language'),
  TOGGLE_SIDEBAR: UtilService.checkActionType('[Settings] Toggle Sidebar'),
};

/**
 * Settings Actions
 */
export class SetLanguageAction implements Action {
  public type = SettingsActionTypes.SET_LANGUAGE;

  constructor(public payload: string) { }
}

export class ToggleSidebarAction implements Action {
  public type = SettingsActionTypes.TOGGLE_SIDEBAR;

  constructor() { }
}

export type SettingsActions =
  | ToggleSidebarAction
  | SetLanguageAction;
