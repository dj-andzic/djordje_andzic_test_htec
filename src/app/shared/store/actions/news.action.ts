import { Action } from '@ngrx/store';

import { UtilService } from "../../services/util.service";

export const NewsActionTypes = {
  GET_NEWS:         UtilService.checkActionType('[BaseNewsComponent] Get BaseNewsComponent'),
  GET_NEWS_SUCCESS: UtilService.checkActionType('[BaseNewsComponent] Get BaseNewsComponent Success'),
  GET_NEWS_FAIL:    UtilService.checkActionType('[BaseNewsComponent] Get BaseNewsComponent Fail'),
  LOADING_NEWS:     UtilService.checkActionType('[BaseNewsComponent] LOADING_NEWS'),
  CLEAR_NEWS:       UtilService.checkActionType('[BaseNewsComponent] Clear BaseNewsComponent')
};

/**
 * BaseNewsComponent Actions
 */
export class GetNewsAction implements Action {
  public type = NewsActionTypes.GET_NEWS;

  constructor(public payload?: any) { }
}

export class GetNewsSuccessAction implements Action {
  public type = NewsActionTypes.GET_NEWS_SUCCESS;

  constructor(public payload: any) { }
}

export class GetNewsFailAction implements Action {
  public type = NewsActionTypes.GET_NEWS_FAIL;

  constructor(public payload: any) { }
}

export class LoadingNewsAction implements Action {
  public type = NewsActionTypes.LOADING_NEWS;

  constructor() { }
}

export class ClearNewsAction implements Action {
  public type = NewsActionTypes.CLEAR_NEWS;

  constructor() {}
}

export type NewsActions =
  | GetNewsAction
  | GetNewsSuccessAction
  | GetNewsFailAction
  | LoadingNewsAction
  | ClearNewsAction;
