import {Injectable} from "@angular/core";

import {select, Store} from "@ngrx/store";
import {Observable} from "rxjs";

import {GeneralState} from "./reducers";
import * as fromActions from "./actions";
import * as fromSelectors from './selectors';
import {IArticle, IGetNewsQueryParams} from "../interfaces";

@Injectable()
export class SharedStoreSandbox {
  public $selectedLanguage: Observable<string> = this.store.pipe(select(fromSelectors.getSelectedLanguage));
  public $sideBarOpened: Observable<boolean> = this.store.pipe(select(fromSelectors.getSideBarOpened));
  public $news: Observable<IArticle[]> = this.store.pipe(select(fromSelectors.getArticles));
  public $newsLoading: Observable<boolean> = this.store.pipe(select(fromSelectors.newsLoading));
  public $newsLoaded: Observable<boolean> = this.store.pipe(select(fromSelectors.newsLoaded));
  public $currentUrl: Observable<string> = this.store.pipe(select(fromSelectors.getUrl));
  public $urlParams: Observable<any> = this.store.pipe(select(fromSelectors.getUrlParams));

  constructor(private store: Store<GeneralState>) {
  }

  public setLanguage(lang): void {
    this.store.dispatch(new fromActions.SetLanguageAction(lang));
  }

  public getNews(queryParams?: IGetNewsQueryParams): void {
    this.store.dispatch(new fromActions.GetNewsAction(queryParams));
  }

  public getNewsSuccess(articles: IArticle[]): void {
    this.store.dispatch(new fromActions.GetNewsSuccessAction({ articles }));
  }

  public clearNews(): void {
    this.store.dispatch(new fromActions.ClearNewsAction());
  }

  public setNewsLoading(): void {
    this.store.dispatch(new fromActions.LoadingNewsAction());
  }

  public toggleSideBar(): void {
    this.store.dispatch(new fromActions.ToggleSidebarAction());
  }

  public goBack(): void {
    this.store.dispatch(new fromActions.BackAction());
  }

  public redirect(path: string): void {
    this.store.dispatch(new fromActions.GoAction({ path: [path] }))
  }
}
