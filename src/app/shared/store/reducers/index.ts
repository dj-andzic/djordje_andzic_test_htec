import {ActionReducerMap} from "@ngrx/store";
import {routerReducer} from "@ngrx/router-store";

import * as fromSettings from './settings.reducer';
import * as fromRouter from './router.reducer';
import * as fromNews from './news.reducer';

export interface GeneralState {
  settings: fromSettings.SettingsState;
  router: fromRouter.RouterStateUrl;
  news: fromNews.NewsState
}

export const reducers: ActionReducerMap<GeneralState> = {
  settings: fromSettings.reducer,
  news: fromNews.reducer,
  // @ts-ignore
  router: routerReducer
};
