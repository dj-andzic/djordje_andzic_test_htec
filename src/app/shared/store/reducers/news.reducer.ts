import * as fromNews from '../actions/news.action';
import {IArticle} from "../../interfaces";
import * as endpointsConstants from '../../endpoints-constants';

export interface NewsState {
  newsLoading: boolean;
  newsLoaded: boolean;
  articles: IArticle[];
}

const INITIAL_STATE: NewsState = {
  newsLoading: false,
  newsLoaded: false,
  articles: []
};

export function reducer(
  state = INITIAL_STATE,
  action: fromNews.NewsActions
): NewsState {
  switch (action.type) {
    case fromNews.NewsActionTypes.GET_NEWS:
    case fromNews.NewsActionTypes.LOADING_NEWS: {
      return Object.assign(
        {},
        state,
        {
          newsLoading: true
        }
      );
    }

    case fromNews.NewsActionTypes.GET_NEWS_SUCCESS: {
      let { articles, category } = action.payload;

      articles = articles.map(article => {
        const cat = category || article.category;

        if (cat) {
          const categoryLink = `/category/${cat}`;
          article.articleLink = `${categoryLink}${endpointsConstants.ARTICLE}/${createArticleLink(article)}`;
          article.category = cat;
        } else {
          article.articleLink = `${endpointsConstants.ARTICLE}/${createArticleLink(article)}`;
          article.category = '';
        }

        return article;
      });

      const newState = {
        newsLoading: false,
        newsLoaded: true,
        articles
      };

      return {
        ...newState
      };
    }

    case fromNews.NewsActionTypes.GET_NEWS_FAIL: {
      return Object.assign(
        {},
        state,
        {
          newsLoading: false,
          newsLoaded: false,
          articles: []
        }
      );
    }

    case fromNews.NewsActionTypes.CLEAR_NEWS: {
      return  {
        ...INITIAL_STATE
      };
    }

    default: {
      return state;
    }
  }
}

function createArticleLink(article: IArticle): string {
  return article.title
    .replace(/(\s+|~|`|!|@|#|\$|%|\^|&|\*|\(|\)|{|}|\[|\]|;|:|\"|'|<|,|\.|>|\?|\/|\\|\||-|_|\+|=)/g,'-')
    .replace(/-{2,}/g,'-')
    .toLocaleLowerCase();
}
