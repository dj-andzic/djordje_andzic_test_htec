import * as fromSettings from '../actions/settings.action';

export interface SettingsState {
  selectedLanguage: string;
  sideBarOpened: boolean;
}

const INITIAL_STATE: SettingsState = {
  selectedLanguage: '',
  sideBarOpened: false
};

export function reducer(
  state = INITIAL_STATE,
  action: fromSettings.SettingsActions
): SettingsState {
  switch (action.type) {
    case fromSettings.SettingsActionTypes.SET_LANGUAGE: {
      return Object.assign(
        {},
        state,
        {
          selectedLanguage: action.payload
        }
      );
    }

    case fromSettings.SettingsActionTypes.TOGGLE_SIDEBAR: {
      return Object.assign(
        {},
        state,
        {
          sideBarOpened: !state.sideBarOpened
        }
      );
    }

    default: {
      return state;
    }
  }
}
