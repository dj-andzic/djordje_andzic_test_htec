import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";

import {IArticle, IGetNewsQueryParams} from "../interfaces";
import {HttpService} from "../../core/services/http";
import * as endpointsConstant from '../endpoints-constants';
import {map} from "rxjs/operators";
import {UtilService} from "./util.service";

@Injectable({
  providedIn: 'root'
})
export class NewsService extends HttpService {

  constructor(
    protected http: HttpClient,
    private utilService: UtilService
  ) {
    super(http);
  }

  public getNews(queryParams: IGetNewsQueryParams): Observable<IArticle[]> {
    const newsUrl = this.utilService.setQueryParams(endpointsConstant.TOP_HEADLINES, queryParams);

    return this.get(newsUrl).pipe(map(response => response.articles));
  }
}
