import { Injectable } from '@angular/core';
import { ConfigService } from "../../app-config.service";
import {LocalStorageService} from "../../core/services/core";
import * as fromConstants from '../constants';
import {query} from "@angular/animations";

let typeCache: { [label: string]: boolean } = {};

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private configService: ConfigService, private localStorageService: LocalStorageService) { }

  public get baseUrl(): string {
    const server = this.configService.get('server');
    return `${server.protocol}://${server.host}/${server.version}${server.port ? ':' + server.port : ''}`;
  }

  public get baseApiUrl(): string {
    const apiPath = this.configService.get('server').apiPath;
    return `${this.baseUrl}${apiPath}`;
  }

  public countryApiUrl(requestUrl: string): string {
    const localization = this.configService.get('localization');
    const currentLocale = this.localStorageService.get(fromConstants.LC_KEY_DEFAULT_APP_LANG);
    const country = localization.languages.find(local => local.code === currentLocale);

    if (!requestUrl.includes('?')) {
      return `${requestUrl}?country=${country.shortName}`
    } else {
      return `${requestUrl}&country=${country.shortName}`
    }
  }

  public setQueryParams(url: string, queryParams: any): string {
    if (queryParams) {
      Object.keys(queryParams).forEach(queryParam => {
        if (!url.includes('?')) {
          url += `?${queryParam}=${queryParams[queryParam]}`;
        } else {
          url += `&${queryParam}=${queryParams[queryParam]}`;
        }
      });
    }

    return url;
  }

  public static checkActionType<T>(label: T | ''): T {
    if (typeCache[label as string]) {
      throw new Error(`Action type "${label}" is not unique"`);
    }

    typeCache[label as string] = true;

    return label as T;
  }
}
