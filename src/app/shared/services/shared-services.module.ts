import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UtilService } from './util.service';
import {NewsService} from "./news.service";



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    UtilService,
    NewsService
  ]
})
export class SharedServicesModule { }
