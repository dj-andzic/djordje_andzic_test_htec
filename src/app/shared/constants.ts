export const LC_KEY_DEFAULT_APP_LANG = 'app-lang';

export const CATEGORIES = [
  'Entertainment',
  'General',
  'Health',
  'Science',
  'Sport',
  'Technology'
];

export const TOP_FIVE_PAGE_SIZE = 5;
