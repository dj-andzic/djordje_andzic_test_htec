import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryListComponent } from './category-list/category-list.component';
import {CategoryItemComponent} from "./category-item/category-item.component";
import {CategoryItemArticleComponent} from "./category-item-article/category-item-article.component";

const routes: Routes = [
  {
    path: '',
    component: CategoryListComponent
  },
  {
    path: ':category/article/:title',
    component: CategoryItemArticleComponent
  },
  {
    path: ':category',
    component: CategoryItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
