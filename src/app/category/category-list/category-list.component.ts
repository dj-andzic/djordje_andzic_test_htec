import {ChangeDetectorRef, Component} from '@angular/core';
import {IArticle} from "../../shared/interfaces";
import {concat} from "rxjs";
import {SharedStoreSandbox} from "../../shared/store/shared-store.sandbox";
import {ConfigService} from "../../app-config.service";
import {count, takeUntil} from "rxjs/operators";
import * as fromConstants from "../../shared/constants";
import {NewsService} from "../../shared/services/news.service";
import {BaseNewsComponent} from "../../shared/components/extensibles/base-news.component";

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent extends BaseNewsComponent {
  public country: any;
  private allArticles: IArticle[] = [];
  private initialLoad = true;

  constructor(
    public sharedStoreSandbox: SharedStoreSandbox,
    private configService: ConfigService,
    private newsService: NewsService,
    private cd: ChangeDetectorRef
  ) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    this.sharedStoreSandbox.$news
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(news => {
        if (news.length) {
          this.initialLoad = false;
        }

        this.news = news;
      });

    this.sharedStoreSandbox.$selectedLanguage
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(countryCode => {

        this.clearNews();
        this.getNewsFromAllCategories();

        const languages = this.configService.get('localization').languages;

        for (let i = 0; i < languages.length; i++) {
          if (countryCode === languages[i].code) {
            this.country = { country: languages[i].country };
            this.cd.detectChanges();
            break;
          }
        }
      });
  }

  private getNewsFromAllCategories(): void {
    this.sharedStoreSandbox.setNewsLoading();

    concat(
      this.newsService.getNews({ category: fromConstants.CATEGORIES[0], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE }),
      this.newsService.getNews({ category: fromConstants.CATEGORIES[1], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE }),
      this.newsService.getNews({ category: fromConstants.CATEGORIES[2], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE }),
      this.newsService.getNews({ category: fromConstants.CATEGORIES[3], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE }),
      this.newsService.getNews({ category: fromConstants.CATEGORIES[4], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE }),
      this.newsService.getNews({ category: fromConstants.CATEGORIES[5], pageSize: fromConstants.TOP_FIVE_PAGE_SIZE })
    ).pipe(
      takeUntil(this.ngUnsubscribe$),
      count((articles, i) => {
        articles.forEach(article => {
          article.category = fromConstants.CATEGORIES[i];
        });

        this.allArticles = [...this.allArticles, ...articles];
        return i % 6 === 1;
      })
    ).subscribe(() => {
      this.sharedStoreSandbox.getNewsSuccess(this.allArticles);
    });
  }

  private clearNews(): void {
    this.sharedStoreSandbox.clearNews();
    this.allArticles.length = 0;
  }
}
