import {ChangeDetectorRef, Component} from '@angular/core';

import {takeUntil} from "rxjs/operators";

import {SharedStoreSandbox} from "../../shared/store/shared-store.sandbox";
import {ConfigService} from "../../app-config.service";
import {BaseNewsComponent} from "../../shared/components/extensibles/base-news.component";

@Component({
  selector: 'app-category-item',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.scss']
})
export class CategoryItemComponent extends BaseNewsComponent {
  public categoryLabel: any;
  public country: any;
  private urlParams: any;
  private initialLoad = true;

  constructor(
    public sharedStoreSandbox: SharedStoreSandbox,
    private configService: ConfigService,
    private cd: ChangeDetectorRef
  ) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    this.sharedStoreSandbox.clearNews();

    this.sharedStoreSandbox.$news
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(news => {
        if (news.length) {
          this.initialLoad = false;
        }

        this.news = news;
      });

    this.sharedStoreSandbox.$urlParams
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(urlParams => {
        this.urlParams = urlParams;

        if (!this.news.length) {
          this.sharedStoreSandbox.getNews({category: urlParams.category});
        }

        this.categoryLabel = { categoryLabel: urlParams.category };
      });

    this.sharedStoreSandbox.$selectedLanguage
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe(countryCode => {

        if (!this.initialLoad) {
          this.sharedStoreSandbox.getNews({category: this.urlParams.category});
        }

        const languages = this.configService.get('localization').languages;

        for (let i = 0; i < languages.length; i++) {
          if (countryCode === languages[i].code) {
            this.country = { country: languages[i].country };
            this.cd.detectChanges();
            break;
          }
        }
      });
  }
}
