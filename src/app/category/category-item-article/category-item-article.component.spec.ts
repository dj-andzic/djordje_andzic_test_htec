import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryItemArticleComponent } from './category-item-article.component';

describe('CategoryItemArticleComponent', () => {
  let component: CategoryItemArticleComponent;
  let fixture: ComponentFixture<CategoryItemArticleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryItemArticleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryItemArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
