import {Component} from '@angular/core';
import {combineLatest} from "rxjs";
import {IArticle} from "../../shared/interfaces";
import {SharedStoreSandbox} from "../../shared/store/shared-store.sandbox";
import {takeUntil} from "rxjs/operators";
import {BaseComponent} from "../../shared/components/extensibles/base.component";

@Component({
  selector: 'app-category-item-article',
  templateUrl: './category-item-article.component.html',
  styleUrls: ['./category-item-article.component.scss']
})
export class CategoryItemArticleComponent extends BaseComponent {
  public article: IArticle;

  constructor(public sharedStoreSandbox: SharedStoreSandbox) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    combineLatest(
      this.sharedStoreSandbox.$urlParams,
      this.sharedStoreSandbox.$news
    )
      .pipe(takeUntil(this.ngUnsubscribe$))
      .subscribe((data: [any, IArticle[]]) => {
        const [urlParams, articles] = data;

        if (!articles.length) {
          this.sharedStoreSandbox.getNews({category: urlParams.category});
        } else {
          this.article = articles.find(article => {
            return article.articleLink.includes(urlParams.title);
          });

          if (!this.article) {
            this.unsubscribeAll();
            this.sharedStoreSandbox.redirect('/not-found');
          } else {
            this.unsubscribeAll();
          }
        }
      });
  }

  private unsubscribeAll(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
