import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryListComponent } from './category-list/category-list.component';
import {CategoryItemComponent} from "./category-item/category-item.component";
import {TranslateModule} from "@ngx-translate/core";
import {SharedComponentsModule} from "../shared/components/shared-components.module";
import {AngularMaterialComponentsModule} from "../shared/components/angular-material-components/angular-material-components.module";
import { CategoryItemArticleComponent } from './category-item-article/category-item-article.component';


@NgModule({
  declarations: [
    CategoryListComponent,
    CategoryItemComponent,
    CategoryItemArticleComponent
  ],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    TranslateModule,
    AngularMaterialComponentsModule,
    SharedComponentsModule
  ]
})
export class CategoryModule { }
