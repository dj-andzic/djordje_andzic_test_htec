import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  NavigationCancel,
  NavigationEnd
} from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

import {
  LocalStorageService,
  LoggerService
} from "./core/services/core";
import * as fromConstants from './shared/constants';
import {ConfigService} from "./app-config.service";
import {SharedStoreSandbox} from "./shared/store/shared-store.sandbox";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public showLoader = false;

  constructor(
    private translate: TranslateService,
    private localStorageService: LocalStorageService,
    private configService: ConfigService,
    public sharedStoreSandbox: SharedStoreSandbox,
    private loggerService: LoggerService,
    private router: Router
  ) {}

  ngOnInit(): void {
    let lang = this.localStorageService.get(fromConstants.LC_KEY_DEFAULT_APP_LANG);

    if (!lang) {
      lang = this.configService.get('localization').defaultLanguage;
      this.localStorageService.set(fromConstants.LC_KEY_DEFAULT_APP_LANG, lang);
    }

    this.translate.setDefaultLang(lang);
    this.sharedStoreSandbox.setLanguage(lang);

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationCancel) {
        this.loggerService.routeLog(`Route ${event.url} canceled`);
      }

      if (event instanceof NavigationEnd) {
        this.loggerService.routeLog(`Route changed to: ${event.url}`);
      }
    });

    this.sharedStoreSandbox.$newsLoading.subscribe(loading => {
      setTimeout(() => {
        this.showLoader = loading;
      });
    });
  }

  public toggleSideBar(): void {
    this.sharedStoreSandbox.toggleSideBar();
  }
}
