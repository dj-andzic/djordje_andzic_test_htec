import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from "./shared/components/not-found/not-found.component";


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'top-news'
  },
  {
    path: 'top-news',
    loadChildren: () => import('./top-news/top-news.module').then(m => m.TopNewsModule)
  },
  {
    path: 'article/:title',
    loadChildren: () => import('./article/article.module').then(m => m.ArticleModule)
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: 'category',
    loadChildren: () => import('./category/category.module').then(m => m.CategoryModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then(m => m.SearchModule)
  },
  {
    path: '**',
    redirectTo: 'top-news'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
