import { BrowserModule }           from '@angular/platform-browser';
import {
  APP_INITIALIZER,
  NgModule
} from '@angular/core';
import {
  HttpClientModule,
  HttpClient
} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule }            from '@angular/router';

import { FlexLayoutModule }        from '@angular/flex-layout';
import {
  TranslateModule,
  TranslateLoader
}                                  from '@ngx-translate/core';
import { TranslateHttpLoader }     from '@ngx-translate/http-loader';
import { StoreModule }             from "@ngrx/store";
import { EffectsModule }           from "@ngrx/effects";
import {StoreRouterConnectingModule} from "@ngrx/router-store";
import { StoreDevtoolsModule }     from "@ngrx/store-devtools";

import { AppRoutingModule }        from './app-routing.module';
import { SharedComponentsModule }  from './shared/components/shared-components.module';
import { AppComponent }            from './app.component';
import { SharedServicesModule }    from './shared/services/shared-services.module';
import { ConfigService }           from './app-config.service';
import { environment }             from "../environments/environment";
import { effects }                 from './shared/store/effects';
import { reducers }                from "./shared/store/reducers";
import { CustomSerializer }        from "./shared/store/reducers/router.reducer";
import {HttpServiceModule} from "./core/services/http";

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './config/i18n/', '.json');
}

export function configServiceFactory(config: ConfigService) {
  return () => config.load();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    RouterModule,
    SharedComponentsModule,
    SharedServicesModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot({
      serializer: CustomSerializer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
    HttpServiceModule.forRoot()
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
