import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {TranslateModule} from "@ngx-translate/core";

import { ArticleRoutingModule } from './article-routing.module';
import { ArticleComponent } from './article.component';
import {AngularMaterialComponentsModule} from "../shared/components/angular-material-components/angular-material-components.module";
import {SharedComponentsModule} from "../shared/components/shared-components.module";


@NgModule({
  declarations: [ArticleComponent],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    AngularMaterialComponentsModule,
    TranslateModule,
    SharedComponentsModule
  ]
})
export class ArticleModule { }
