import {Component} from '@angular/core';

import {takeUntil} from "rxjs/operators";
import {combineLatest} from "rxjs";

import {IArticle} from "../shared/interfaces";
import {SharedStoreSandbox} from "../shared/store/shared-store.sandbox";
import {BaseComponent} from "../shared/components/extensibles/base.component";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent extends BaseComponent {
  public article: IArticle;

  constructor(public sharedStoreSandbox: SharedStoreSandbox) {
    super(sharedStoreSandbox);
  }

  ngOnInit(): void {
    combineLatest(
      this.sharedStoreSandbox.$urlParams,
      this.sharedStoreSandbox.$news
    )
    .pipe(takeUntil(this.ngUnsubscribe$))
    .subscribe((data: [any, IArticle[]]) => {
      const [urlParams, articles] = data;

      if (!articles.length) {
        this.sharedStoreSandbox.getNews();
      } else {
        this.article = articles.find(article => {
          return article.articleLink.replace('/article/', '') === urlParams.title;
        });

        if (!this.article) {
          this.unsubscribeAll();
          this.sharedStoreSandbox.redirect('/not-found');
        } else {
          this.unsubscribeAll();
        }
      }
    });
  }

  private unsubscribeAll(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
